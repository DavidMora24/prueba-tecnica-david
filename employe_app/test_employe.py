from fastapi.testclient import TestClient

from .crud import get_employees
from .database import SessionLocal
from .main import app


employe = TestClient(app)


def test_read_employees():
    response = employe.get("/employees/")
    assert response.status_code == 200


def test_read_employ():
    db = SessionLocal()
    all_employees = get_employees(db, skip=0, limit=100)
    if len(all_employees) > 0:
        response = employe.get(f"/employees/{all_employees[0].code}")
        assert response.status_code == 200
    else:
        response = employe.get("/employees/9999")
        assert response.status_code == 404
