from sqlalchemy.orm import Session
from . import models, schemas
from datetime import date, datetime


def get_employe_by_code(db: Session, code: str):
    """ Get an employee by code """
    return db.query(models.Employ).filter(models.Employ.code == code)


def get_employees(db: Session, skip: int = 0, limit: int = 100):
    """ Get a list of employees """
    return db.query(models.Employ).offset(skip).limit(limit).all()


def get_employees_by_date(db: Session, choise_date: int = date.today()):
    """ Get a list of employees from date """
    choise_date_str = datetime.strptime(choise_date, '%Y-%m-%d')
    return db.query(models.Employ).filter(models.Employ.date_incorp > choise_date_str).all()


def create_employ(db: Session, employ: schemas.EmployCreate):
    """ Create employees """
    db_employ = models.Employ(
        code=employ.code,
        name=employ.name,
        last_name=employ.last_name,
        email=employ.email,
        nif=employ.nif,
        date_incorp=employ.date_incorp
        )
    db.add(db_employ)
    db.commit()
    db.refresh(db_employ)
    return db_employ


def delete_employe_by_code(db: Session, employ: schemas.EmployDelete, code):
    """ Delete employees """
    db_employ = get_employe_by_code(db, code=code).first()
    db.delete(db_employ)
    db.commit()


def modify_employe_by_code(db: Session, employ: schemas.EmployModify, code, name, last_name, email, nif, date_incorp):
    """ Modify employees """
    db_employ = get_employe_by_code(db, code=code).first()
    name = db_employ.name if name is None else name
    last_name = db_employ.last_name if last_name is None else last_name
    email = db_employ.email if email is None else email
    nif = db_employ.nif if nif is None else nif
    date_incorp = db_employ.date_incorp if date_incorp is None else date_incorp
    get_employe_by_code(db, code=code).update(
        {
            'name': name,
            'last_name': last_name,
            'email': email,
            'nif': nif,
            'date_incorp': date_incorp,
        })
    db.commit()