from typing import List, Optional

from fastapi import Depends, FastAPI, HTTPException
from sqlalchemy.orm import Session

from . import crud, models, schemas
from .database import SessionLocal, engine

models.Base.metadata.create_all(bind=engine)

app = FastAPI()


# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.get("/employees/", response_model=List[schemas.Employ])
def read_employees(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    employees = crud.get_employees(db, skip=skip, limit=limit)
    if len(employees) <= 0:
        raise HTTPException(status_code=404, detail="Employees list empty")
    return employees


@app.get("/employees/{code}", response_model=schemas.Employ)
def read_employ(code: int, db: Session = Depends(get_db)):
    db_employ = crud.get_employe_by_code(db, code=code).first()
    if db_employ is None:
        raise HTTPException(status_code=404, detail="Employe with code specified not found")
    return db_employ

    
@app.get("/employees_by_date/", response_model=List[schemas.Employ])
def read_employees(choise_date: str = None, db: Session = Depends(get_db)):
    employees = crud.get_employees_by_date(db, choise_date=choise_date)
    if len(employees) <= 0:
        raise HTTPException(status_code=404, detail="Employees list empty")
    return employees


@app.post("/employees/", response_model=schemas.Employ)
def create_employ(employ: schemas.EmployCreate, db: Session = Depends(get_db)):
    db_employ = crud.get_employe_by_code(db, code=employ.code).first()
    if db_employ:
        raise HTTPException(status_code=400, detail="Employe already registered with code informed")
    return crud.create_employ(db=db, employ=employ)


@app.put("/employe/{code}")
def modify_employe(
    code: int,
    name: Optional[str] = None,
    last_name: Optional[str] = None,
    email: Optional[str] = None,
    nif: Optional[str] = None,
    date_incorp: Optional[str] = None,
    db: Session = Depends(get_db)
    ):
    db_employ = crud.get_employe_by_code(db, code=code).first()

    if db_employ is None:
        raise HTTPException(status_code=404, detail="Employ not found")
    return crud.modify_employe_by_code(
        db=db,
        employ=db_employ,
        code=code,
        name=name,
        last_name=last_name,
        email=email,
        nif=nif,
        date_incorp=date_incorp
        )


@app.delete("/employe/{code}")
def delete_employe(code: int, db: Session = Depends(get_db)):
    db_employ = crud.get_employe_by_code(db, code=code).first()
    if db_employ is None:
        raise HTTPException(status_code=404, detail="Employe with code specified not found")
    return crud.delete_employe_by_code(db=db, employ=db_employ, code=code)
