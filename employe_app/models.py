from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Date, CheckConstraint
from sqlalchemy.orm import relationship, validates
from datetime import date, datetime

from .database import Base


class Employ(Base):
    __tablename__ = "employees"

    id = Column(Integer, primary_key=True, index=True)
    code = Column(Integer, unique=True,)
    name = Column(String)
    last_name = Column(String)
    email = Column(String, index=True)
    nif = Column(String, unique=True)
    date_incorp = Column(String)


    @validates('email')
    def validate_email(self, key, email):
        if '@' not in email or len(email) < 6 or '.' not in email:
            raise ValueError("failed simple email validation")
        return email
    

    @validates('nif')
    def validate_nif(self, key, nif):
        options_letter = ["T", "R", "W", "A", "G", "M", "Y", "F", "P", 
                        "D", "X", "B", "N", "J", "Z", "S", "Q", "V", "H",
                        "L", "C", "K", "E"]
        numeric = nif[:-1]
        letter = nif[-1]
        total = 0
        for n in numeric:
            total += int(n)
        if len(nif) != 9 or options_letter[total % 23] != letter:
            raise ValueError("failed NIF validation")
        return nif
    

    @validates('date_incorp')
    def validate_date_incorp(self, key, date_incorp):
        current_day = datetime.strftime(date.today(), '%Y-%m-%d')
        format_valid = False
        try:
            datetime.strptime(date_incorp, '%Y-%m-%d')
            format_valid = True
        except ValueError:
            raise ValueError("date_incorp format not correct - must be '9999-99-99'")
        if format_valid and date_incorp > current_day:
            return date_incorp
        else:
            raise ValueError("failed date_incorp validation: past date not allowed")






