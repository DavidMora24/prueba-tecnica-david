from typing import List, Optional
from pydantic import BaseModel


class EmployBase(BaseModel):
    code: int
    name: str
    last_name: str
    email: str
    nif: str
    date_incorp: str


class EmployCreate(EmployBase):
    pass


class EmployDelete(EmployBase):
    pass


class EmployModify(EmployBase):
    pass


class Employ(EmployBase):
    id: int

    class Config:
        orm_mode = True

